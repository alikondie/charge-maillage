﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using UnityEngine;
using System.Globalization;

public class Loader : MonoBehaviour {

    int numberOfVerts = 0;
    int numberOffaces = 0;
    string path = "";
    Vector3[] vertices;
    Vector3 center;
    public Material mat;
    int[] triangles;
    void getProperties()
    {
        path = "Assets/Resources/Models/buddha.off";

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);
        reader.ReadLine(); // read oFF
        string properties = reader.ReadLine();
        int i = 0;
        string verts = "";
        while(properties[i] != ' ')
        {
            verts += properties[i];
            i++;
        }
        i++;
        numberOfVerts = Int32.Parse(verts);
        string faces = "";
        while (properties[i] != ' ')
        {
            faces += properties[i];
            i++;
        }
        numberOffaces = Int32.Parse(faces);
        reader.Close();
    }

    void Load()
    {
        StreamReader reader = new StreamReader(path);
        string verts = "";
        string faces = "";
        reader.ReadLine();
        reader.ReadLine(); // read the two first lines.
        vertices = new Vector3[numberOfVerts];
        triangles = new int[numberOffaces * 3];
        // start to fill the verts lines
        for (int i=0; i < numberOfVerts; i++)
        {
            verts = reader.ReadLine();
            int j = 0;
            string v1S = "";
            string v2S = "";
            string v3S = "";
            while (verts[j] != ' ')
            {
                v1S += verts[j];
                j++;
            }
            j++;
            while (verts[j] != ' ')
            {
                v2S += verts[j];
                j++;
            }
            j++;
            while (j < verts.Length)
            {
                v3S += verts[j];
                j++;
            }
            float v1 = Convert.ToSingle(v1S);
            float v2 = Convert.ToSingle(v2S);
            float v3 = Convert.ToSingle(v3S);
            //Debug.Log(v1);
            vertices[i] = new Vector3(v1, v2, v3);

        }
        //Debug.Log(vertices[numberOfVerts - 1].ToString("F4"));
        
        
        for (int i = 0; i < numberOffaces * 3; i+=3)
        {
            faces = reader.ReadLine();
            string t1S = "";
            string t2S = "";
            string t3S = "";
            int j = 2;
            while (faces[j] != ' ')
            {
                t1S += faces[j];
                j++;
            }
            j++;
            while (faces[j] != ' ')
            {
                t2S += faces[j];
                j++;
            }
            j++;
            while (j < faces.Length)
            {
                t3S += faces[j];
                j++;
            }
            triangles[i] = Int32.Parse(t1S);
            triangles[i+1] = Int32.Parse(t2S);
            triangles[i+2] = Int32.Parse(t3S);
        }
        //Debug.Log(faces[numberOffaces - 1].ToString());
    }

    void Center()
    {
        Vector3 sum = Vector3.zero;
        for(int i=0; i < numberOfVerts; i++)
        {
            sum += vertices[i];
        }
        sum = sum / numberOfVerts;

        transform.position = sum;
        center = sum;
        
    }
    void Normalize()
    {
        float max = 0;
        for (int i = 0; i < numberOfVerts; i++)
        {
            if (vertices[i].magnitude > max)
                max = vertices[i].magnitude;
        }
        for (int i = 0; i < numberOfVerts; i++)
        {
            vertices[i] /= max;
        }
    }

    void Start()
    {
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        getProperties();
        Load();
        Center();
        Normalize();
        gameObject.GetComponent<MeshRenderer>().material = mat;
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }
}
