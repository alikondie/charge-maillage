﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Partitonnement : MonoBehaviour {

    // Use this for initialization
    int[] triangles;
    Vector3[] vertices;
    public int x = 5;
    public int y = 5;
    public Material mat;
    void Start () {
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        gameObject.GetComponent<MeshRenderer>().material = mat;
        Initialize();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void Initialize()
    {
        vertices = new Vector3[25];
        int counter = 0;
        // making vertices
        for(int i = 0; i < y; i++)
        {
            for(int j = 0;j < x; j++)
            {
                vertices[counter] = new Vector3(x, y, 0);
                counter++;
            }
        }
        // making triangles
        triangles = new int[(x - 1) * (y - 1) * 6];
        int f = 0;
        for(int i = 0; i < y * x; i++)
        {
            
            if ((i + 1) % y == 0 || (i + 1) % x == 0)
                continue;

            Debug.Log(i);
            triangles[f] = i;
            f++;
            triangles[f] = x + i;
            f++;
            triangles[f] = x + i + 1;
            f++;
            triangles[f] = i;
            f++;
            triangles[f] = i + 1;
            f++;
            triangles[f] = x + i + 1;
            f++;
        }
    }
}
